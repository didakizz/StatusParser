﻿using System;

namespace StatusParser
{
    public class Status : IStatus
    {
        private string socialNetwork;
        private User user;
        private Message message;

        public Status(string socialNetwork, User user, Message message)
        {
            this.socialNetwork = socialNetwork;
            this.user = user;
            this.message = message;
        }

        public string SocialNetwork
        {
            get
            {
                return socialNetwork;
            }
            set
            {
                socialNetwork = value;
            }
        }

        public User User
        {
            get
            {
                return user;
            }
            set
            {
                user = value;
            }
        }

        public Message Message
        {
            get
            {
                return message;
            }
            set
            {
                message = value;
            }
        }

        public override string ToString()
        {
            return string.Format("{0}'s said '{1}' on {2} \n {3}",
                                 User.Name, Message.Text, SocialNetwork, Message.DateReceived);
        }
    }
}
