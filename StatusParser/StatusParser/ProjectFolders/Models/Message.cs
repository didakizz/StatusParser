﻿using System;

namespace StatusParser
{
    public class Message : IMessage
    {
        private string text;
        private string dateReceived;

        public Message(string text, string dateReceived)
        {
            this.text = text;
            this.dateReceived = dateReceived;
        }

        public string Text
        {
            get
            {
                return text;
            }
            set
            {
                text = value;
            }
        }

        public string DateReceived
        {
            get
            {
                return dateReceived;
            }
            set
            {
                dateReceived = value;
            }
        }
    }
}
