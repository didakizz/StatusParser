﻿namespace StatusParser
{
    public interface IMessage
    {
        string Text { get; set; }
        string DateReceived { get; set; }
    }
}