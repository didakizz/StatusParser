﻿namespace StatusParser
{
    public interface IUser
    {
        string Name { get; set; }
        string ImageUrl { get; set; }
    }
}