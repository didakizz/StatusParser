﻿namespace StatusParser
{
    public interface IStatus
    {
        string SocialNetwork { get; set; }
        User User { get; set; }
        Message Message { get; set; }
    }
}