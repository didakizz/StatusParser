﻿using System;

namespace StatusParser
{
    public class User : IUser
    {
        private string name;
        private string imageUrl;

        public User(string name, string imageUrl)
        {
            this.name = name;
            this.imageUrl = imageUrl;
        }

        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }

        public string ImageUrl
        {
            get
            {
                return imageUrl;
            }
            set
            {
                imageUrl = value;
            }
        }

    }
}