﻿using Newtonsoft.Json;
using System;
using System.Net;

namespace StatusParser
{
    /// <summary>
    /// The Networking Manager class.
    /// Contains all methods for performing statuses loading and image downloading functions.
    /// </summary>
    public class NetworkingManager
    {
        /// <summary>
        /// Loads Status objects from JSON object.
        /// </summary>
        /// <returns>
        /// The statuses deserialized in an Array of Status objects.
        /// </returns>
        /// <param name="json">The JSON object to deserialize the Status objects from.</param>
        public IStatus[] LoadStatusesFromJson(string json)
        {
            try
            {
                var statuses = JsonConvert.DeserializeObject<Status[]>(json);
                return statuses;

            }
            catch (JsonException jsonException)
            {
                Console.WriteLine("Failed to deserialize json object with json exception: /n {0}!", jsonException);
            }
            return new Status[0];
        }

        /// <summary>
        /// Download an image with a given URL to a file location.
        /// </summary>
        /// <param name="url">The URL of the image to be downloaded.</param>
        /// <param name="fileLocation">The file location the image to be downloaded to.</param>
        public void DownloadImage(string url, string fileLocation)
        {
            using (WebClient webClient = new WebClient())
            {
                try
                {
                    webClient.DownloadFile(url, fileLocation);
                }
                catch (WebException webException)
                {
                    Console.WriteLine("Failed to download image webException: /n {0}!", webException);
                }
            }
        }
    }
}
