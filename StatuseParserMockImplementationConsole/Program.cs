﻿using System;
using System.IO;
using StatusParser;

namespace StatuseParserMockImplementationConsole
{
    static class Constants
    {
        public const string yesAnswer = "yes";
        public const string noAnswer = "no";
        public const string jsonExample = @"
                  [{
                    'SocialNetwork': 'Social Network Name',
                    'User': {
                      'Name': 'Peter',
                      'ImageUrl': 'some URL'
                    },
                    'Message': {
                      'Text': 'Example message text',
                      'DateReceived': '2/8/2018 1:25 PM'
                    '}
                  }],";
        
        public const string json = @"[
  {
    'SocialNetwork': 'Facebook',
    'User': {
      'Name': 'Donald',
      'ImageUrl': 'https://img.huffingtonpost.com/asset/59355ce52300003b00348d3f.jpeg?ops=scalefit_720_noupscale'
    },
    'Message': {
      'Text': 'Hey, I am the worst president ever',
      'DateReceived': '2/8/2018 1:25 PM'
    }
  },
  {
    'SocialNetwork': 'Twitter',
    'User': {
      'Name': 'Sisi',
      'ImageUrl': 'https://static.pexels.com/photos/104827/cat-pet-animal-domestic-104827.jpeg'
    },
    'Message': {
      'Text': 'Meow, Meow, need food!',
      'DateReceived': '2/4/2018 1:10 AM'
    }
  },
  {
    'SocialNetwork': 'LinkedIn',
    'User': {
      'Name': 'Evil Recruiter',
      'ImageUrl': 'https://media-exp2.licdn.com/mpr/mpr/AAEAAQAAAAAAAAUOAAAAJGU3MjE0Y2UyLTI2MmUtNDhkNi1hMzgxLWQ0N2IwMDA2Y2M4ZQ.jpg'
    },
    'Message': {
      'Text': 'Coming to get you!!!',
      'DateReceived': '2/3/2018 1:25 PM'
    }
  },
  {
    'SocialNetwork': 'Instagram',
    'User': {
      'Name': 'Annoying Foodie',
      'ImageUrl': 'https://www.thecollegeview.com/wp-content/uploads/2017/09/man-eating-spaghetti-foods-make-you-hungrier.jpg'
    },
    'Message': {
      'Text': 'Check out my dish!',
      'DateReceived': '2/1/2018 8:25 PM'
    }
  }
]";

    }

    class MainClass
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Hey, there, this is a simple mock implementation of my little library! \nThe json received has this shape:");
            Console.WriteLine(Constants.jsonExample);
            Console.WriteLine("The console app just received some mock data...");

            // 1. Load networking Manager
            NetworkingManager networkingManager = new NetworkingManager();

            // 2. Load Statuses
            var statuses = networkingManager.LoadStatusesFromJson(Constants.json);

            if (statuses.Length > 0)
            {
                LoadStatuses(statuses);
            }
            else
            {
                Console.WriteLine("No statuses to show!");
            }

            // 3. Download images
            DownLoadImagesForStatuses(networkingManager, statuses);
        }

        private static void DownLoadImagesForStatuses(NetworkingManager networkingManager, IStatus[] statuses)
        {
            Console.WriteLine("Download the pictures too? yes or no..");

            var picturesAnswer = Console.ReadLine().ToLower();
            var picturesAnswerInvalid = true;

            while (picturesAnswerInvalid)
            {
                if (picturesAnswer == "yes")
                {
                    for (int i = 0; i < statuses.Length; i++)
                    {
                        networkingManager.DownloadImage(statuses[i].User.ImageUrl, string.Format("randompic{0}.jpg", i));
                    }
                    Console.WriteLine("Find your downloaded pictures in the project's Debug folder!");
                    picturesAnswerInvalid = false;
                }
                else if (picturesAnswer == "no")
                {
                    Console.WriteLine("Alright, program will exit now.");
                    picturesAnswerInvalid = false;
                }
                else
                {
                    Console.WriteLine("Only yes or no answer is accepted for downloading pictures...");
                    picturesAnswer = Console.ReadLine().ToLower();
                }
            }
        }

        private static void LoadStatuses(IStatus[] statuses)
        {
            Console.WriteLine("Friends' statuses successfully loaded from json file. \nDo you want to see all statuses' info? Answer yes or no..");

            var answer = Console.ReadLine().ToLower();
            var answerInvalid = true;

            while (answerInvalid)
            {
                if (answer == "yes")
                {
                    foreach (var status in statuses)
                    {
                        Console.WriteLine(status);
                    }
                    answerInvalid = false;
                }
                else if (answer == "no")
                {
                    Console.WriteLine("Ok, maybe you want to download some pictures?");
                    answerInvalid = false;
                }
                else
                {
                    Console.WriteLine("Only yes or no answer is accepted if you want to see statuses..");
                    answer = Console.ReadLine().ToLower();
                }
            }
        }

        private static string CheckFilePathEmpty(string jsonFilePath)
        {
            while (jsonFilePath == "")
            {
                Console.WriteLine("File path should not be empty");
                jsonFilePath = Console.ReadLine();
            }

            return jsonFilePath;
        }
    }
}
